'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify= require('gulp-uglify'),
    usemin= require('gulp-usemin'),
    rev= require('gulp-rev'),
    cleanCss= require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

gulp.task('sass', function () {
    return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function () {
    var files = [
        './*.html',
        './css/*.css',
        './img/*.{png,jpg,gif}',
        './js/*.js'
    ];

    browserSync.init(files, {
        server: {
            baseDir: "./"
        }
    });

});

// Default task
gulp.task('default', gulp.series('browser-sync', 'sass:watch'));

gulp.task('clean', function () {
   return del(['dist']);
});

gulp.task('copyfonts', function () {
    return gulp.src('./node_modules/font-awesome/fonts/**/*.{ttf,woff,eof,svg}*')
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('imagemin', function () {
   return gulp.src('img/*.{png,jpg,gif}')
   .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
   .pipe(gulp.dest('dist/img'));
});

gulp.task('usemin', function () {
   return gulp.src('./*.html')
   .pipe(flatmap(function (stream, file) {
       return stream
       .pipe(usemin({
           css: [rev()],
           html: [function () {return htmlmin({ collapseWhitespace: true})}],
           js: [uglify(), rev()],
           inlinejs: [uglify()],
           inlinecss: [cleanCss(), 'concat']
       }))
   }))
   .pipe(gulp.dest('dist/'));
   //Flatmap takes these multiple htmlfiles and then starts up parallel pipelines for each one of these htmlfiles. Each one of them going through the same set of steps and then finally, converging and copying it into the destination folder.
  // the css part will obviously do the concatenation and minification and so on, and then applies the rev to that so that it acts that 20 bit string to the main.css file there.
   // usemin task takes the htmlfiles and then looks up the CSS and JavaScript blocks in the htmlfiles, combines, concatenates, and minifies and nuglifies the files and then replaces them by using the concatenated file in the distribution folder.
});

gulp.task('build', gulp.series('clean', 'copyfonts', 'imagemin', 'usemin', function (done) { done(); }
));